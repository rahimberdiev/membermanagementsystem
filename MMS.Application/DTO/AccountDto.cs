﻿using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Application.DTO
{
    public class AccountDto
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string Name { get; set; }
        public int Balance { get; set; }
        public string Status { get; set; }
        public AccountDto() { }
        public AccountDto(Account account)
        {
            Id = account.Id;
            MemberId = account.MemberId;
            Name = account.Name;
            Balance = account.Balance.Value;
            Status = account.Status.Name;
        }
        public Account GetAccount()
        {
            var account = Account.NewAccount(MemberId, Name, new Point(Balance), BaseEnumeration.GetByNameOrAlias<AccountStatus>(Status));
            return account;
        }
    }
}
