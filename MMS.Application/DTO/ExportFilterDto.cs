﻿using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Application.DTO
{
    public class ExportFilterDto
    {
        public int? MinBalance { get; set; }
        public int? MaxBalance { get; set; }
        public string Status { get; set; }
        public string AccountName { get; set; }
        public string MemberName { get; set; }
        public string MemberAddress { get; set; }

        public ExportFilter GetExportFilter()
        {
            return new ExportFilter(MemberName,
                MemberAddress != null ? new MemberAddress(MemberAddress) : null,
                AccountName,
                Status != null ? BaseEnumeration.GetByNameOrAlias<AccountStatus>(Status) : null,
                MinBalance != null ? new Point(MinBalance.Value) : null,
                MaxBalance != null ? new Point(MaxBalance.Value) : null);
        }
    }
}
