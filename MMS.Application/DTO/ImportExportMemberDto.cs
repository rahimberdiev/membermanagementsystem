﻿using MMS.Domain.AggregatesModel.MemberAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Application.DTO
{
    public class ImportExportMemberDto : MemberDto
    {
        public List<AccountDto> Accounts { get; set; } = new List<AccountDto>();
        public ImportExportMemberDto()
        { }
        public ImportExportMemberDto(Member member):base(member)
        {
            foreach(var account in member.GetAccounts())
            {
                Accounts.Add(new AccountDto(account));
            }
        }
    }
}
