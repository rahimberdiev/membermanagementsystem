﻿using MMS.Domain.AggregatesModel.MemberAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Application.DTO
{
    public class MemberDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public MemberDto() { }
        public MemberDto(Member member)
        {
            Id = member.Id;
            Name = member.Name;
            Address = member.Address.ToString();
        }
        public Member GetMember()
        {   
            var member = Member.NewMember(Name,new MemberAddress(Address));
            return member;
        }
    }
}
