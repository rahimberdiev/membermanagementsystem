﻿using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.OperationAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Application.DTO
{
    public class NewOperationDto
    {
        public int AccountId { get; set; }
        public int Amount { get; set; }
    }
}
