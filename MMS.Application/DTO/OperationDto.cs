﻿using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.OperationAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Application.DTO
{
    public class OperationDto
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public int AccountId { get; set; }
        public int Amount { get; set; }
        public string OperationType { get; set; }
        public OperationDto() { }
        public OperationDto(Operation operation)
        {
            Id = operation.Id;
            MemberId = operation.MemberId;
            AccountId = operation.AccountId;
            Amount = operation.Amount.Value;
            OperationType = operation.OperationType.Name;
        }

        public Operation GetOperation()
        {
            var operation = Operation.NewOperation(AccountId,
                MemberId,
                new Point(Amount),
                BaseEnumeration.GetByNameOrAlias<OperationType>(OperationType));
            return operation;
        }

    }
}
