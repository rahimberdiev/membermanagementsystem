﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Threading;
using System.Threading.Tasks;

namespace MMS.Application.Handlers
{
    class AddAccountRequestHandler : IRequestHandler<AddAccountRequest, AccountDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMembersManager _membersManager;
        public AddAccountRequestHandler(IUnitOfWork unitOfWork, IMembersManager membersManager) => (_unitOfWork, _membersManager) = (unitOfWork, membersManager);

        public async Task<AccountDto> Handle(AddAccountRequest request, CancellationToken cancellationToken)
        {
            using (_unitOfWork)
            {
                var member = await _membersManager.GetMemberAsync(request.AccountDto.MemberId);
                var newAccount = member.AddNewAccount(request.AccountDto.GetAccount());
                await _unitOfWork.CommitAsync();
                return new AccountDto(newAccount);
            }
        }
    }
}
