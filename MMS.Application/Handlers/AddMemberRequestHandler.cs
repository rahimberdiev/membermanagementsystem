﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Threading;
using System.Threading.Tasks;

namespace MMS.Application.Handlers
{
    class AddMemberRequestHandler : IRequestHandler<AddMemberRequest, MemberDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMembersManager _membersManager;
        public AddMemberRequestHandler(IUnitOfWork unitOfWork, IMembersManager membersManager) => (_unitOfWork, _membersManager) = (unitOfWork, membersManager);

        public async Task<MemberDto> Handle(AddMemberRequest request, CancellationToken cancellationToken)
        {
            using (_unitOfWork)
            {
                var member = request.MemberDto.GetMember();
                var newMember = await _membersManager.AddMemberAsync(member);
                await _unitOfWork.CommitAsync();
                return new MemberDto(newMember);
            }
        }
    }
}
