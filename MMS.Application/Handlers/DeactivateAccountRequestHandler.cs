﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Threading;
using System.Threading.Tasks;

namespace MMS.Application.Handlers
{
    class DeactivateAccountRequestHandler : IRequestHandler<DeactivateAccountRequest, AccountDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountsManager _accountsManager;
        public DeactivateAccountRequestHandler(IUnitOfWork unitOfWork, IAccountsManager accountsManager) => (_unitOfWork, _accountsManager) = (unitOfWork, accountsManager);

        public async Task<AccountDto> Handle(DeactivateAccountRequest request, CancellationToken cancellationToken)
        {
            using (_unitOfWork)
            {
                var account = await _accountsManager.GetAccountAsync(request.AccountId);
                account.Deactivate();
                await _unitOfWork.CommitAsync();
                return new AccountDto(account);
            }
        }
    }
}
