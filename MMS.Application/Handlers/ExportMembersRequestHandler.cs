﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;

namespace MMS.Application.Handlers
{
    class ExportMembersRequestHandler : IRequestHandler<ExportMembersRequest, IEnumerable<ImportExportMemberDto>>
    {
        private readonly IMembersManager _membersManager;
        public ExportMembersRequestHandler(IMembersManager membersManager) => _membersManager = membersManager;

        public async Task<IEnumerable<ImportExportMemberDto>> Handle(ExportMembersRequest request, CancellationToken cancellationToken)
        {
            var members = await _membersManager.GetAllMembersAsync(new ExportSpecification(request.Filter.GetExportFilter()));

            var result = new List<ImportExportMemberDto>();
            foreach (var member in members)
            {
                if (member.Accounts.Count > 0)
                    result.Add(new ImportExportMemberDto(member));
            }
            return result;
        }
    }
}
