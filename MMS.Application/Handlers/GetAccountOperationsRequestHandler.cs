﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using MMS.Domain.AggregatesModel.AccountAggregate;

namespace MMS.Application.Handlers
{
    class GetAccountOperationsRequestHandler : IRequestHandler<GetAccountOperationsRequest, IEnumerable<OperationDto>>
    {
        private readonly IAccountsManager _accountsManager;
        public GetAccountOperationsRequestHandler(IAccountsManager accountsManager) => _accountsManager = accountsManager;
        public async Task<IEnumerable<OperationDto>> Handle(GetAccountOperationsRequest request, CancellationToken cancellationToken)
        {
            var member = await _accountsManager.GetAccountWithOperationsAsync(request.AccountId);
            var operations = member.GetOperations();
            return operations.Select(operation => new OperationDto(operation));
        }
    }
}
