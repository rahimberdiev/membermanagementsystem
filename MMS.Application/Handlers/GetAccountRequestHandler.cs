﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
namespace MMS.Application.Handlers
{
    class GetAccountRequestHandler : IRequestHandler<GetAccountRequest, AccountDto>
    {
        private readonly IAccountsManager _accountsManager;
        public GetAccountRequestHandler(IAccountsManager accountsManager) =>
            (_accountsManager) = (accountsManager);

        public async Task<AccountDto> Handle(GetAccountRequest request, CancellationToken cancellationToken)
        {
            var account = await _accountsManager.GetAccountAsync(request.AccountId);
            return new AccountDto(account);
        }
    }
}
