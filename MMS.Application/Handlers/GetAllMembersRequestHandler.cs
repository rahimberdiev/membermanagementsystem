﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
namespace MMS.Application.Handlers
{
    class GetAllMembersRequestHandler : IRequestHandler<GetAllMembersRequest, IEnumerable<MemberDto>>
    {
        private readonly IMembersManager _memberManager;
        public GetAllMembersRequestHandler(IMembersManager memberManager) => _memberManager = memberManager;
        public async Task<IEnumerable<MemberDto>> Handle(GetAllMembersRequest request, CancellationToken cancellationToken)
        {
            var members = await _memberManager.GetAllMembersAsync();
            return members.Select(member => new MemberDto(member));
        }
    }
}
