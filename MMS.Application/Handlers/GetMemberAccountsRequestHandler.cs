﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
namespace MMS.Application.Handlers
{
    class GetMemberAccountsRequestHandler : IRequestHandler<GetMemberAccountsRequest, IEnumerable<AccountDto>>
    {
        private readonly IMembersManager _memberManager;
        public GetMemberAccountsRequestHandler(IMembersManager memberManager) => _memberManager = memberManager;
        public async Task<IEnumerable<AccountDto>> Handle(GetMemberAccountsRequest request, CancellationToken cancellationToken)
        {
            var member = await _memberManager.GetMemberWithAccountsAsync(request.MemberId);
            var accounts = member.GetAccounts();
            return accounts.Select(account => new AccountDto(account));
        }
    }
}
