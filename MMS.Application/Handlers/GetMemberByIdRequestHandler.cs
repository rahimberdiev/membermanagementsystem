﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Threading;
using System.Threading.Tasks;

namespace MMS.Application.Handlers
{
    class GetMemberByIdRequestHandler : IRequestHandler<GetMemberByIdRequest, MemberDto>
    {
        private readonly IMembersManager _memberManager;
        public GetMemberByIdRequestHandler(IMembersManager memberManager) => _memberManager = memberManager;
        public async Task<MemberDto> Handle(GetMemberByIdRequest request, CancellationToken cancellationToken)
        {
            var member = await _memberManager.GetMemberAsync(request.MemberId);
            return new MemberDto(member);
        }
    }
}
