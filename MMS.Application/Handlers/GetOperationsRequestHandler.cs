﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
namespace MMS.Application.Handlers
{
    class GetOperationsRequestHandler : IRequestHandler<GetOperationsRequest, IEnumerable<OperationDto>>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountsManager _accountsManager;
        public GetOperationsRequestHandler(IUnitOfWork unitOfWork, IAccountsManager accountsManager) =>
            (_unitOfWork, _accountsManager) = (unitOfWork, accountsManager);

        public async Task<IEnumerable<OperationDto>> Handle(GetOperationsRequest request, CancellationToken cancellationToken)
        {
            var account = await _accountsManager.GetAccountWithOperationsAsync(request.AccountId);
            var operations = account.GetOperations();
            return operations.Select(operation => new OperationDto(operation));
        }
    }
}
