﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Threading;
using System.Threading.Tasks;

namespace MMS.Application.Handlers
{
    class ImportMembersRequestHandler : IRequestHandler<ImportMembersRequest, bool>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMembersManager _membersManager;
        public ImportMembersRequestHandler(IUnitOfWork unitOfWork, IMembersManager membersManager) => (_unitOfWork, _membersManager) = (unitOfWork, membersManager);

        public async Task<bool> Handle(ImportMembersRequest request, CancellationToken cancellationToken)
        {
            using (_unitOfWork)
            {
                foreach(ImportExportMemberDto memberDto in request.Members)
                {
                    var account = await _membersManager.AddMemberAsync(memberDto.GetMember());
                    foreach(AccountDto accountDto in memberDto.Accounts)
                    {
                        account.AddNewAccount(accountDto.GetAccount());
                    }                    
                }
                await _unitOfWork.CommitAsync();
                return true;
            }
        }
    }
}
