﻿using MediatR;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System.Threading;
using System.Threading.Tasks;

namespace MMS.Application.Handlers
{
    class RedeemOperationRequestHandler : IRequestHandler<RedeemOperationRequest, OperationDto>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountsManager _accountsManager;
        public RedeemOperationRequestHandler(IUnitOfWork unitOfWork, IAccountsManager accountsManager) =>
            (_unitOfWork, _accountsManager) = (unitOfWork, accountsManager);

        public async Task<OperationDto> Handle(RedeemOperationRequest request, CancellationToken cancellationToken)
        {
            using (_unitOfWork)
            {
                var account = await _accountsManager.GetAccountAsync(request.RedeemOperationDto.AccountId);
                var operation = account.Redeem(new Point(request.RedeemOperationDto.Amount));
                await _unitOfWork.CommitAsync();
                return new OperationDto(operation);
            }
        }
    }
}
