﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;

namespace MMS.Application.Requests
{
    public class ActivateAccountRequest : IRequest<AccountDto>
    {
        public ActivateAccountRequest(int accountId) => (AccountId) = (accountId);
        public int AccountId { get; set; }
    }
}
