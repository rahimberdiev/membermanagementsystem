﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;

namespace MMS.Application.Requests
{
    public class AddAccountRequest : IRequest<AccountDto>
    {
        public AddAccountRequest(AccountDto accountDto) => (AccountDto) = (accountDto);
        public AccountDto AccountDto { get; set; }
    }
}
