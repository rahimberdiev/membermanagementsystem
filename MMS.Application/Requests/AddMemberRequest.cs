﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;

namespace MMS.Application.Requests
{
    public class AddMemberRequest : IRequest<MemberDto>
    {
        public AddMemberRequest(MemberDto memberDto) => MemberDto = memberDto;
        public MemberDto MemberDto { get; set; }
    }
}
