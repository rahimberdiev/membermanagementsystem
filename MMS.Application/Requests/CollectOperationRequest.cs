﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;

namespace MMS.Application.Requests
{
    public class CollectOperationRequest : IRequest<OperationDto>
    {
        public CollectOperationRequest(NewOperationDto collectOperationDto) => (CollectOperationDto) = (collectOperationDto);
        public NewOperationDto CollectOperationDto { get; set; }
    }
}
