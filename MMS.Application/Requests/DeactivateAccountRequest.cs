﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;

namespace MMS.Application.Requests
{
    public class DeactivateAccountRequest : IRequest<AccountDto>
    {
        public DeactivateAccountRequest(int accountId) => (AccountId) = (accountId);
        public int AccountId { get; set; }
    }
}
