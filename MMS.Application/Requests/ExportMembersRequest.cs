﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;
using System.Collections.Generic;

namespace MMS.Application.Requests
{
    public class ExportMembersRequest : IRequest<IEnumerable<ImportExportMemberDto>>
    {
        public ExportMembersRequest(ExportFilterDto filter) => (Filter) = (filter);
        public ExportFilterDto Filter { get; set; }
    }
}
