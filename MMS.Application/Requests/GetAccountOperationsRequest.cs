﻿using MediatR;
using MMS.Application.DTO;
using System.Collections.Generic;

namespace MMS.Application.Requests
{
    public class GetAccountOperationsRequest : IRequest<IEnumerable<OperationDto>>
    {
        public GetAccountOperationsRequest(int accountId) => AccountId = accountId;
        public int AccountId { get; set; }
    }
}
