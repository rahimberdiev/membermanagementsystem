﻿using MediatR;
using MMS.Application.DTO;

namespace MMS.Application.Requests
{
    public class GetAccountRequest : IRequest<AccountDto>
    {
        public GetAccountRequest(int accountId) => (AccountId) = (accountId);
        public int AccountId { get; set; }
    }
}
