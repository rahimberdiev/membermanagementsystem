﻿using MediatR;
using MMS.Application.DTO;
using System.Collections.Generic;

namespace MMS.Application.Requests
{
    public class GetAllMembersRequest:IRequest<IEnumerable<MemberDto>>{ }
}
