﻿using MediatR;
using MMS.Application.DTO;
using System.Collections.Generic;

namespace MMS.Application.Requests
{
    public class GetMemberAccountsRequest : IRequest<IEnumerable<AccountDto>>
    {
        public GetMemberAccountsRequest(int memberId) => MemberId = memberId;
        public int MemberId { get; set; }
    }
}
