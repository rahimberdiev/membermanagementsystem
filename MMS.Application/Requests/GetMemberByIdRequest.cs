﻿using MediatR;
using MMS.Application.DTO;

namespace MMS.Application.Requests
{
    public class GetMemberByIdRequest : IRequest<MemberDto>
    {
        public GetMemberByIdRequest(int memberId) => MemberId = memberId;
        public int MemberId { get; set; }
    }
}
