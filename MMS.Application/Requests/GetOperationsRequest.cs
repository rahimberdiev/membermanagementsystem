﻿using MediatR;
using MMS.Application.DTO;
using System.Collections.Generic;

namespace MMS.Application.Requests
{
    public class GetOperationsRequest : IRequest<IEnumerable<OperationDto>>{
        public int AccountId { get; set; }
        public GetOperationsRequest(int accountId) => AccountId = accountId;
    }
}
