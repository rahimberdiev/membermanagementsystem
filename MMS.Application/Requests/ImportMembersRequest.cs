﻿using MediatR;
using MMS.Application.DTO;
using System.Collections.Generic;

namespace MMS.Application.Requests
{
    public class ImportMembersRequest : IRequest<bool>
    {
        public ImportMembersRequest(IEnumerable<ImportExportMemberDto> members) => (Members) = (members);
        public IEnumerable<ImportExportMemberDto> Members { get; set; }
    }
}
