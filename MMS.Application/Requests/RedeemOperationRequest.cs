﻿using MediatR;
using MMS.Application.DTO;
using MMS.Domain.AggregatesModel.MemberAggregate;

namespace MMS.Application.Requests
{
    public class RedeemOperationRequest : IRequest<OperationDto>
    {
        public RedeemOperationRequest(NewOperationDto redeemDto) => (RedeemOperationDto) = (redeemDto);
        public NewOperationDto RedeemOperationDto { get; set; }
    }
}
