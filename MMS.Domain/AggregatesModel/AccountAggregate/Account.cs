﻿using MMS.Domain.AggregatesModel.OperationAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MMS.Domain.AggregatesModel.AccountAggregate
{
    public class Account : IAggregateRoot<int>
    {
        public int Id { get; private set; }
        public int MemberId { get; set; }
        public string Name { get; private set; }
        public Point Balance { get; private set; }
        public AccountStatus Status { get; private set; }
        public DateTime OpenDate { get; private set; }
        private List<Operation> Operations { get; set; } = new List<Operation>();

        public static Account NewAccount(int memberId,string name, Point balance, AccountStatus status)
        {
            return new Account() { MemberId = memberId, Name = name, Balance = balance, Status = status, OpenDate = DateTime.Now };
        }
        public void Activate()
        {
            Status = AccountStatus.ACTIVE;
        }
        public void Deactivate()
        {
            Status = AccountStatus.INACTIVE;
        }
        public Operation Redeem(Point amount)
        {
            CheckAccount();
            lock (_lockObject)
            {
                var remain = Balance.subtract(amount);
                if (remain.IsValid())
                {
                    var newOperation = Operation.NewOperation(Id, MemberId, amount, OperationType.REDEEM);
                    Operations.Add(newOperation);
                    this.Balance = remain;
                    return newOperation;
                }
                else
                    throw new InvalidOperationException();
            }
        }
        public Operation Collect(Point amount)
        {
            CheckAccount();

            var newOperation = Operation.NewOperation(Id, MemberId, amount, OperationType.COLLECT);
            Operations.Add(newOperation);
            Balance = Balance.add(amount);
            return newOperation;
        }

        public List<Operation> GetOperations()
        {
            return Operations.ToList();
        }
        protected void CheckAccount()
        {
            if (Status == AccountStatus.INACTIVE)
                throw new InvalidOperationException();
        }
        private object _lockObject = new object();
    }
}
