﻿using System;
using System.Collections.Generic;
using System.Text;
using MMS.Domain.SeedWork;
namespace MMS.Domain.AggregatesModel.AccountAggregate
{
    public class AccountStatus : BaseEnumeration
    {
        public static AccountStatus INACTIVE = new AccountStatus(1, nameof(INACTIVE));
        public static AccountStatus ACTIVE = new AccountStatus(1, nameof(ACTIVE));

        protected AccountStatus(int id, string name)
            : base(id, name)
        {
        }
        protected AccountStatus(int id, string name, List<string> aliases)
            : base(id, name, aliases)
        {
        }
    }
}
