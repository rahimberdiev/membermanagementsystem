﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.AccountAggregate.Specifications;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;

namespace MMS.Domain.AggregatesModel.AccountAggregate
{
    public class AccountsManager: IAccountsManager
    {
        private readonly IAccountsRepository _repo;
        public AccountsManager(IAccountsRepository repo)
        {
            _repo = repo;
        }        

        public async Task<IEnumerable<Account>> GetAccountsAsync(int memberId)
        {
            return await _repo.GetAllAsync(new MemberAccountsSpecification(memberId));
        }

        public async Task<Account> GetAccountAsync(int accountId)
        {
            return await _repo.GetByIdAsync(accountId);
        }

        public async Task<Account> GetAccountWithOperationsAsync(int accountId)
        {
            return await _repo.GetBySpecificationAsync(new AccountWithOperationsSpecification(accountId));
        }
    }
}
