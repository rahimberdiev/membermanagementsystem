﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;

namespace MMS.Domain.AggregatesModel.AccountAggregate
{
    public interface IAccountsManager : IDomainObject
    {
        Task<IEnumerable<Account>> GetAccountsAsync(int memberId);
        Task<Account> GetAccountAsync(int accountId);
        Task<Account> GetAccountWithOperationsAsync(int accountId);
    }
}
