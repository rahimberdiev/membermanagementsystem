﻿using MMS.Domain.SeedWork;

namespace MMS.Domain.AggregatesModel.AccountAggregate
{
    public interface IAccountsRepository : IRepository<Account, int>
    {
    }
}
