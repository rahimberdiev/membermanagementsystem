﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.AccountAggregate.Specifications
{
    public class AccountWithOperationsSpecification : BaseSpecification<Account>
    {
        public AccountWithOperationsSpecification(int accountId)
            : base(account => account.Id == accountId)
        {
            AddInclude("Operations");
        }
    }
}