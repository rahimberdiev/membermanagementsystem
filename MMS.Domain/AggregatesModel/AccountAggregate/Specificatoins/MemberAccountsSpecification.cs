﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.AccountAggregate.Specifications
{
    public class MemberAccountsSpecification : BaseSpecification<Account>
    {
        public MemberAccountsSpecification(int memberId)
            : base(account => account.MemberId == memberId)
        {
        }
    }
}