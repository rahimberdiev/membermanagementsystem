﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;

namespace MMS.Domain.AggregatesModel.MemberAggregate
{
    public interface IMembersManager : IDomainObject
    {
        Task<IEnumerable<Member>> GetAllMembersAsync();
        Task<IEnumerable<Member>> GetAllMembersAsync(ISpecification<Member> specification);
        Task<Member> GetMemberAsync(int memberId);
        Task<Member> GetMemberWithAccountsAsync(int memberId);
        Task<Member> AddMemberAsync(Member member);
    }
}
