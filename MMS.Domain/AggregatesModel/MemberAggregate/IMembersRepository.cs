﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.MemberAggregate
{
    public interface IMembersRepository : IRepository<Member, int>
    {
    }
}
