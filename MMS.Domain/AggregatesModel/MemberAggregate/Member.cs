﻿using System;
using System.Collections.Generic;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.SeedWork;
using System.Linq;

namespace MMS.Domain.AggregatesModel.MemberAggregate
{
    public class Member : IAggregateRoot<int>
    {        
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DateTime RegistrationDate { get; private set; }
        public MemberAddress Address { get; private set; }
        public List<Account> Accounts { get; private set; } = new List<Account>();

        public Account AddNewAccount(Account account)
        {
            Accounts.Add(account);
            return account;
        }
        public IEnumerable<Account> GetAccounts()
        {
            var accounts = Accounts.ToList();
            return accounts;
        }
        public static Member NewMember(string name, MemberAddress address)
        {
            return new Member() { Name = name, Address = address, RegistrationDate = DateTime.Now };
        }
    }
}
