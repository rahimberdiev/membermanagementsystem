﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.MemberAggregate
{
    public class MemberAddress : IValueObject
    {
        private string _address;
        public string Address => _address;

        public MemberAddress(string address)
        {
            _address = address;
        }

        #region Equals - GetHashCode - ToString - CompareTo
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            MemberAddress other = obj as MemberAddress;
            if (other != null)
                return this._address.CompareTo(other._address);
            else
                throw new InvalidCastException();
        }
        public bool Equals(MemberAddress other) => _address == other._address;
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MemberAddress)obj);
        }
        public override int GetHashCode() => _address.GetHashCode();
        public override string ToString() => _address;
        #endregion 
    }
}
