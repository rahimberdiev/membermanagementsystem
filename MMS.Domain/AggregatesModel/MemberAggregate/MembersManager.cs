﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;

namespace MMS.Domain.AggregatesModel.MemberAggregate
{
    public class MembersManager : IMembersManager
    {
        private readonly IMembersRepository _repo;
        public MembersManager(IMembersRepository repo)
        {
            _repo = repo;
        }

        public async Task<Member> AddMemberAsync(Member member)
        {
            return await _repo.AddAsync(member);
        }

        public async Task<IEnumerable<Member>> GetAllMembersAsync()
        {
            return await _repo.GetAllAsync(new AllMembersSpecification());
        }

        public async Task<IEnumerable<Member>> GetAllMembersAsync(ISpecification<Member> specification)
        {
            return await _repo.GetAllAsync(specification);
        }

        public async Task<Member> GetMemberAsync(int memberId)
        {
            return await _repo.GetByIdAsync(memberId);
        }

        public async Task<Member> GetMemberWithAccountsAsync(int memberId)
        {
            return await _repo.GetBySpecificationAsync(new MemberWithAccountsSpecification(memberId));
        }
    }
}
