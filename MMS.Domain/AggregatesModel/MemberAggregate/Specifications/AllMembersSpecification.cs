﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.MemberAggregate.Specifications
{
    public class AllMembersSpecification : BaseSpecification<Member>
    {
        public AllMembersSpecification()
            : base(member => true)
        {
        }
    }
}