﻿using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MMS.Domain.AggregatesModel.MemberAggregate.Specifications
{
    public class ExportSpecification : BaseSpecification<Member>
    {
        public ExportSpecification(ExportFilter filter)
        {
            Expression<Func<Member, bool>> filterExpression = null;

            if (filter.MemberName != null)
            {
                Expression<Func<Member, bool>> nameCriteria = member => member.Name == filter.MemberName;
                if (filterExpression == null)
                    filterExpression = nameCriteria;
                else
                    filterExpression = filterExpression.AndAlso(nameCriteria);
            }
            if (filter.MemberAddress != null)
            {
                Expression<Func<Member, bool>> addressCriteria = member => member.Address.Address == filter.MemberAddress.Address;
                if (filterExpression == null)
                    filterExpression = addressCriteria;
                else
                    filterExpression = filterExpression.AndAlso(addressCriteria);
            }
            if (filterExpression == null) filterExpression = member => true;

            InitCritiria(filterExpression);
            InitIncludes(filter);
        }
        private void InitIncludes(ExportFilter filter)
        {
            Expression<Func<Account, bool>> expression = null;
            if (filter.AccountName != null)
            {
                Expression<Func<Account, bool>> nameCriteria = account => account.Name == filter.AccountName;
                if (expression == null)
                    expression = nameCriteria;
                else
                    expression = expression.AndAlso(nameCriteria);
            }
            if (filter.AccountStatus != null)
            {
                Expression<Func<Account, bool>> statusCriteria = account => account.Status == filter.AccountStatus;
                if (expression == null)
                    expression = statusCriteria;
                else
                    expression = expression.AndAlso(statusCriteria);
            }
            if (filter.MinBalance != null)
            {
                Expression<Func<Account, bool>> minCriteria = account => account.Balance.CompareTo(new Point(filter.MinBalance.Value)) > 0;
                if (expression == null)
                    expression = minCriteria;
                else
                    expression = expression.AndAlso(minCriteria);
            }
            if (filter.MaxBalance != null)
            {
                Expression<Func<Account, bool>> maxCriteria = account => account.Balance.CompareTo(new Point(filter.MaxBalance.Value)) < 0;
                if (expression == null)
                    expression = maxCriteria;
                else
                    expression = expression.AndAlso(maxCriteria);
            }
            if (expression != null)
                AddInclude(member => member.Accounts.AsQueryable().Where(expression));
            else
                AddInclude(member => member.Accounts);
        }
    }    
}
