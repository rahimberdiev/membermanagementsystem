﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.OperationAggregate
{
    public class Operation : IAggregateRoot<int>
    {
        public int Id { get; private set; }
        public int MemberId { get; private set; }
        public int AccountId { get; private set; }
        public Point Amount { get; private set; }
        public OperationType OperationType { get; private set; }
        public DateTime OperationDate { get; private set; }

        public static Operation NewOperation(int accountId, int memberId, Point amount, OperationType operationType)
        {
            return new Operation() { AccountId = accountId, MemberId = memberId, Amount = amount, OperationDate = DateTime.Now, OperationType = operationType };
        }
    }
}
