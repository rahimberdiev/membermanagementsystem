﻿using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.AggregatesModel.OperationAggregate
{
    public class OperationType : BaseEnumeration
    {   
        public static OperationType COLLECT = new OperationType(1, nameof(COLLECT));
        public static OperationType REDEEM = new OperationType(2, nameof(REDEEM));

        protected OperationType(int id, string name)
            : base(id, name)
        {
        }
        protected OperationType(int id, string name, List<string> aliases)
            : base(id, name, aliases)
        {
        }
    }
}
