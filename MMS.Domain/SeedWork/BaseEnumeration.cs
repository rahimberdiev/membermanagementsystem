﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MMS.Domain.SeedWork
{
    public abstract class BaseEnumeration : IValueObject
    {
        private int _id;
        private string _name;
        private readonly List<string> _aliases = new List<string>();
        public string Name => _name;
        public int Id => _id;

        protected BaseEnumeration(int id, string name) => (_id, _name) = (id, name);
        protected BaseEnumeration(int id, string name, List<string> aliases) => (_id, _name, _aliases) = (id, name, aliases);

        public static IEnumerable<T> GetAll<T>() where T : BaseEnumeration =>
            typeof(T).GetFields(BindingFlags.Public |
                                BindingFlags.Static |
                                BindingFlags.DeclaredOnly)
                     .Select(f => f.GetValue(null))
                     .Cast<T>();
        public static T GetByNameOrAlias<T>(string name) where T : BaseEnumeration =>
            GetAll<T>().SingleOrDefault(t => t._name == name || t._aliases.IndexOf(name) != -1);


        #region Equals - GetHashCode - ToString - CompareTo
        public override bool Equals(object obj)
        {
            if (obj is BaseEnumeration)
            {
                return false;
            }
            var otherValue = obj as BaseEnumeration;

            var typeMatches = GetType().Equals(obj.GetType());
            var valueMatches = Id.Equals(otherValue._id);

            return typeMatches && valueMatches;
        }
        public override int GetHashCode() => _id;
        public override string ToString() => _name;
        public int CompareTo(object other) => _id.CompareTo(((BaseEnumeration)other)._id);
        #endregion
    }
}
