﻿using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.SeedWork
{
    public class ExportFilter
    {
        public string MemberName { get; private set; }
        public MemberAddress MemberAddress { get; private set; }
        public string AccountName { get; private set; }
        public AccountStatus AccountStatus { get; private set; }
        public Point MinBalance { get; private set; }
        public Point MaxBalance { get; private set; }
        
        public ExportFilter(string memberName, MemberAddress memberAddress, string accountName, AccountStatus accountStatus, Point minBalance, Point maxBalance)
        {
            MemberName = memberName;
            MemberAddress = memberAddress;
            AccountName = accountName;
            AccountStatus = accountStatus;
            MinBalance = maxBalance;
            MaxBalance = maxBalance;
        }
    }
}
