﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MMS.Domain.SeedWork
{
    public interface IRepository<TEntity, ID> where TEntity : IEntity<ID>
    {
        Task<TEntity> AddAsync(TEntity entity);
        void Remove(TEntity entity);
        Task<TEntity> GetByIdAsync(ID id);
        Task<IEnumerable<TEntity>> GetAllAsync(ISpecification<TEntity> spec);
        Task<TEntity> GetBySpecificationAsync(ISpecification<TEntity> spec);
    }
}
