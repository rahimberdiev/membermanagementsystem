﻿using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MMS.Domain.SeedWork
{
    public interface IUnitOfWork: IDisposable
    {
        Task CommitAsync();
    }
}
