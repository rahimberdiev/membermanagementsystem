﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.SeedWork
{
    public interface IValueObject : IDomainObject, IComparable
    {
        bool Equals(object obj);
        int GetHashCode();
        string ToString();
    }
}
