﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Domain.SeedWork
{
    public class Point:IValueObject
    {
        private readonly int _value;
        public Point(int amount) => _value = amount;
        public int Value => _value;
        public Point add(Point other) => new Point(_value+other._value);
        public Point subtract(Point other) => new Point(_value - other._value);
        public bool IsValid()
        {
            return _value >= 0;
        }
        #region Equals - GetHashCode - ToString - CompareTo
        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Point other = obj as Point;
            if (other != null)
                return this._value.CompareTo(other._value);
            else
                throw new InvalidCastException();
        }
        public bool Equals(Point other) => _value == other._value;
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Point)obj);
        }
        public override int GetHashCode() => _value.GetHashCode();
        public override string ToString() => _value.ToString();
        #endregion 

    }
}
