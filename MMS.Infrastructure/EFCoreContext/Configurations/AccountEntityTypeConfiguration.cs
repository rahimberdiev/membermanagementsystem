﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Infrastructure.EFCoreContext.Configurations
{
    class AccountEntityTypeConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> accountConfiguration)
        {
            accountConfiguration.ToTable("accounts", MMSContext.DEFAULT_SCHEMA);

            accountConfiguration.HasKey(account=>account.Id)
                .HasName("PK_AccountId");

            accountConfiguration.Property(account=>account.Id).HasColumnName("Id");

            accountConfiguration
                .Property(account=>account.Name)
                .HasColumnName("Name")
                .IsRequired(true);

            accountConfiguration
                .Property(account => account.Status)
                .HasColumnName("Status")
                .HasDefaultValue(AccountStatus.ACTIVE)
                .IsRequired(true)
                .HasConversion(
                    v => v.Name,
                    v => AccountStatus.GetByNameOrAlias<AccountStatus>(v));

            accountConfiguration
                .Property(account=>account.MemberId)
                .HasColumnName("MemberId")
                .IsRequired(true);

            accountConfiguration
                .Property(account=>account.Balance)
                .HasColumnName("Balance")
                .IsRequired(true)
                .HasDefaultValue(new Point(0))
                .HasConversion(
                    v => v.Value,
                    v => new Point(v));

            accountConfiguration.HasMany("Operations")
               .WithOne()
               .HasForeignKey("AccountId")
               .IsRequired(true)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
