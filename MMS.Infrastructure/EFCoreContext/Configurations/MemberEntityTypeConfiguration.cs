﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Infrastructure.EFCoreContext.Configurations
{
    class MemberEntityTypeConfiguration : IEntityTypeConfiguration<Member>
    {
        public void Configure(EntityTypeBuilder<Member> memberConfiguration)
        {
            memberConfiguration.ToTable("members", MMSContext.DEFAULT_SCHEMA);

            memberConfiguration.HasKey(member => member.Id);

            memberConfiguration
                .OwnsOne(o => o.Address, a =>
                {
                    a.Property(p => p.Address).HasColumnName("MemberAddress");
                });

            memberConfiguration
                .Property(m=>m.Name)
                .UsePropertyAccessMode(PropertyAccessMode.Field)
                .HasColumnName("Name")
                .IsRequired(true);


            memberConfiguration.HasMany("Accounts")
                .WithOne()
                .HasForeignKey("MemberId")
                .IsRequired(true)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
