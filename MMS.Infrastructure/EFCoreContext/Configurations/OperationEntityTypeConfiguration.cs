﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.OperationAggregate;
using MMS.Domain.SeedWork;

namespace MMS.Infrastructure.EFCoreContext.Configurations
{
    class OperationEntityTypeConfiguration : IEntityTypeConfiguration<Operation>
    {
        public void Configure(EntityTypeBuilder<Operation> memberConfiguration)
        {
            memberConfiguration.ToTable("operations", MMSContext.DEFAULT_SCHEMA);

            memberConfiguration.HasKey(op=>op.Id)
                .HasName("PK_OperationId");

            memberConfiguration.Property(op=>op.Id).HasColumnName("Id");

            memberConfiguration
                .Property(op=>op.OperationDate)
                .HasDefaultValueSql("getdate()")
                .HasColumnName("OperationDate")
                .IsRequired(true);

            memberConfiguration
                .Property(op=>op.OperationType)
                .HasColumnName("Status")
                .HasConversion(
                    v => v.Name,
                    v => BaseEnumeration.GetByNameOrAlias<OperationType>(v));

            memberConfiguration
                .Property(op=>op.AccountId)
                .HasColumnName("AccountId")
                .IsRequired(true);

            memberConfiguration
                .Property(op=>op.Amount)
                .HasColumnName("Amount")
                .HasPrecision(14, 2)
                .IsRequired(true)
                .HasConversion(
                    v => v.Value,
                    v => new Point(v));
        }
    }
}
