﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.IO;
using MMS.Infrastructure.EFCoreContext.Configurations;
namespace MMS.Infrastructure.EFCoreContext
{
    public class MMSContext : DbContext
    {
        public const string DEFAULT_SCHEMA = "MMS";
        public DbSet<Member> Members { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = GetConnectionString();
            optionsBuilder.UseSqlite(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MemberEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AccountEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OperationEntityTypeConfiguration());
        }
        private string GetConnectionString()
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(@Directory.GetCurrentDirectory() + "/../MMS.WebApi/appsettings.json").Build();
            return configuration.GetConnectionString("DatabaseConnection");
        }
    }
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<MMSContext>
    {
        public MMSContext CreateDbContext(string[] args)
        {   
            return new MMSContext();
        }
    }
}
