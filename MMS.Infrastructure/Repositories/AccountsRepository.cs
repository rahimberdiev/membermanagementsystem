﻿using Microsoft.EntityFrameworkCore;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Infrastructure.EFCoreContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Infrastructure.Repositories
{
    public class AccountsRepository : BaseRepository<Account, int>, IAccountsRepository
    {
        public AccountsRepository(MMSContext context) : base(context)
        {
        }
    }
}
