﻿using Microsoft.EntityFrameworkCore;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMS.Infrastructure.Repositories
{
    public class BaseRepository<TEntity, ID> : IRepository<TEntity, ID> where TEntity : class, IEntity<ID>
    {
        protected readonly DbContext _context;
        public BaseRepository(DbContext context) => _context = context;

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            var newEntity = await _context.Set<TEntity>().AddAsync(entity);
            return newEntity.Entity;
        }

        public async Task<TEntity> GetByIdAsync(ID id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(ISpecification<TEntity> spec)
        {
            var queryableResultWithIncludes = spec.Includes
                .Aggregate(_context.Set<TEntity>().AsQueryable(),
            (current, include) => current.Include(include));

            var secondaryResult = spec.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            return await secondaryResult
                .Where(spec.Criteria).ToListAsync();
        }
        public async Task<TEntity> GetBySpecificationAsync(ISpecification<TEntity> spec)
        {
            var queryableResultWithIncludes = spec.Includes
                .Aggregate(_context.Set<TEntity>().AsQueryable(),
            (current, include) => current.Include(include));

            var secondaryResult = spec.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            return await secondaryResult
                .Where(spec.Criteria).FirstOrDefaultAsync();
        }
        public void Remove(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }
    }
}
