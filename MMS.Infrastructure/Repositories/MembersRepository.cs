﻿using Microsoft.EntityFrameworkCore;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Infrastructure.EFCoreContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace MMS.Infrastructure.Repositories
{
    public class MembersRepository: BaseRepository<Member, int>, IMembersRepository
    {
        public MembersRepository(MMSContext context) : base(context)
        {
        }
    }
}
