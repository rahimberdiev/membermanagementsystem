﻿using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.SeedWork;
using MMS.Infrastructure.EFCoreContext;
using MMS.Infrastructure.Repositories;
using System;
using System.Threading.Tasks;

namespace MMS.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private MMSContext _context;
        public UnitOfWork(MMSContext context)
        {
            _context = context;
        }
        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
