﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMS.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AccountsActivationController : ControllerBase
    {
        private readonly ILogger<MembersController> _logger;
        private readonly IMediator _mediator;
        public AccountsActivationController(ILogger<MembersController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }
        [HttpPut]
        public async Task ActivateAccount(int accountId)
        {
            await _mediator.Send(new ActivateAccountRequest(accountId));
        }
        [HttpPut]
        public async Task DeactivateAccount(int accountId)
        {
            await _mediator.Send(new DeactivateAccountRequest(accountId));
        }
    }
}
