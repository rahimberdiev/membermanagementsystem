﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMS.WebApi.Controllers
{
    [ApiController]
    [Route("account")]
    public class AccountsController : ControllerBase
    {
        private readonly ILogger<MembersController> _logger;
        private readonly IMediator _mediator;
        public AccountsController(ILogger<MembersController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }
        [HttpGet]
        [Route("/accounts/{memberId}")]
        public async Task<IEnumerable<AccountDto>> GetMemberAccountsAsync(int memberId)
        {
            return await _mediator.Send(new GetMemberAccountsRequest(memberId));
        }
        [HttpGet("{id}")]        
        public async Task<AccountDto> GetAccountByIdAsync(int id)
        {
            return await _mediator.Send(new GetAccountRequest(id));
        }
        [HttpPost]
        public async Task<AccountDto> AddAccount(AccountDto accountDto)
        {
            return await _mediator.Send(new AddAccountRequest(accountDto));
        }
    }
}
