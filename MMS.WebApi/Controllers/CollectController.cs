﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.AccountAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMS.WebApi.Controllers
{
    /// <summary>
    /// Controller for credit operations
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class CollectController : ControllerBase
    {
        private readonly ILogger<MembersController> _logger;
        private readonly IMediator _mediator;
        public CollectController(ILogger<MembersController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }
        [HttpPost]
        public async Task<OperationDto> Redeem(NewOperationDto newCollectOperationDto)
        {
            return await _mediator.Send(new CollectOperationRequest(newCollectOperationDto));
        }
    }
}
