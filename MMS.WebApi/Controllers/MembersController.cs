﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMS.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MembersController : ControllerBase
    {
        private readonly ILogger<MembersController> _logger;
        private readonly IMediator _mediator;
        public MembersController(ILogger<MembersController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<MemberDto>> GetAllAsync()
        {
            return await _mediator.Send(new GetAllMembersRequest());
        }
        [HttpGet("{id}")]
        public async Task<MemberDto> GetByIdAsync(int id)
        {
            return await _mediator.Send(new GetMemberByIdRequest(id));
        }
        [HttpPost]
        public async Task<MemberDto> AddMember(MemberDto memberDto)
        {
            return await _mediator.Send(new AddMemberRequest(memberDto));
        }
    }
}
