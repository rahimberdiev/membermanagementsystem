﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MMS.Application.DTO;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Domain.AggregatesModel.MemberAggregate.Specifications;
using MMS.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MMS.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MembersImportExportController : ControllerBase
    {
        private readonly ILogger<MembersController> _logger;
        private readonly IMediator _mediator;
        public MembersImportExportController(ILogger<MembersController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpPost("Import")]
        public async Task ImportMembers(IEnumerable<ImportExportMemberDto> membersDto)
        {
            await _mediator.Send(new ImportMembersRequest(membersDto));
        }
        [HttpGet("Export")]
        public async Task<IEnumerable<ImportExportMemberDto>> ExportByStatus(string memberName, string memberAddress, string accountName, string status, int? minBalance, int? maxBalance)
        {
            return await _mediator.Send(
                new ExportMembersRequest(
                    new ExportFilterDto() { 
                        MemberName = memberName,
                        MemberAddress = memberAddress,
                        AccountName = accountName,
                        Status = status,
                        MinBalance = minBalance,
                        MaxBalance = maxBalance
                    }));
        }

    }
}
