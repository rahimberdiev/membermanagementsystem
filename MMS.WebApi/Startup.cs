using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MMS.Domain.SeedWork;
using MMS.Infrastructure.EFCoreContext;
using MMS.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using MMS.Application.Requests;
using MMS.Domain.AggregatesModel.MemberAggregate;
using MMS.Infrastructure.Repositories;
using MMS.Domain.AggregatesModel.AccountAggregate;

namespace MMS.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "MMS.WebApi", Version = "v1" });
            });
            services.AddDbContext<MMSContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMembersManager, MembersManager>();
            services.AddScoped<IAccountsManager, AccountsManager>();
            services.AddScoped<IMembersRepository, MembersRepository>();
            services.AddScoped<IAccountsRepository, AccountsRepository>();
            services.AddMediatR(typeof(GetAllMembersRequest));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MMS.WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
